export default [



    { path: '/Recommend', name: '推荐', icon: 'smile', component: './Recommend' },
    { path: '/LiveRoom', name: '直播间', icon: 'smile', component: './LiveRoom', hideInMenu: true },


    { path: '/', redirect: '/Recommend' },
    { component: './404' },
];
