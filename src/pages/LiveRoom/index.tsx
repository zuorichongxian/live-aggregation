import { useRequest, request } from 'umi';
import { useMount } from 'ahooks';
import { history } from 'umi';
import Player from '@pansy/react-aliplayer';

const Example: React.FC = () => {
  const {
    data: sourceUrl,
    error,
    loading,
  } = useRequest(() => {
    const {
      location: { query },
    } = history;
    //@ts-ignore
    const { platform, roomId } = query;
    return request(
      `http://live.yj1211.work/api/live/getRealUrl?platform=${platform}&roomId=${roomId}`,
    );
  });
  if (loading) {
    return <div>loading...</div>;
  }
  if (error) {
    return <div>{error.message}</div>;
  }
  //   useMount(() => {
  //     // Update the document title using the browser API
  //     console.log(history);
  //   });
  console.log(sourceUrl);
  //  name: "流畅",url: data.FD,  name: "清晰", url: data.LD,  name: "高清",  url: data.SD,  name: "超清", url: data.HD, name: "原画",url: data.OD,
  return (
    <div style={{ height: 500 }}>
      <Player
        source={sourceUrl.OD}
        // isLive
        options={{
          autoplay: true,
          rePlay: true,
        }}
      />
    </div>
  );
};

export default Example;
