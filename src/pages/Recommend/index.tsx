import { useRequest, request } from 'umi';
import { Card, Avatar } from 'antd';
import { useMount } from 'ahooks';
import { history } from 'umi';
import './index.less';
const { Meta } = Card;

interface roomProps {
  platForm: string;
  roomId: string;
  categoryId: string;
  categoryName: string;
  roomName: string;
  ownerName: string;
  roomPic: string;
  ownerHeadPic: string;
  realUrl: string | null;
  online: number;
  isLive: number;
  isFollowed: number;
  egameToken: string | null;
}

function Recommend() {
  useMount(() => {
    // Update the document title using the browser API
  });
  const {
    data: roomList,
    error,
    loading,
  } = useRequest(() => {
    return request('/api/proxy/live/getRecommend?page=1&size=10');
  });
  if (loading) {
    return <div>loading...</div>;
  }
  if (error) {
    return <div>{error.message}</div>;
  }

  const openRoom = (room: roomProps) => {
    console.log(room);
    history.push({
      pathname: '/liveRoom',
      query: {
        platform: room.platForm,
        roomId: room.roomId,
      },
    });
  };

  return (
    <>
      <div className="room-list">
        {roomList.map((room: roomProps) => (
          <div
            className="room-item"
            key={room.roomId}
            style={{ padding: 20 }}
            onClick={() => {
              openRoom(room);
            }}
          >
            <Card
              key={room.roomId}
              style={{ width: 300, height: 300 }}
              cover={<img style={{ maxHeight: 200 }} alt="example" src={room.roomPic} />}
              actions={
                [
                  // <SettingOutlined key="setting" />,
                  // <EditOutlined key="edit" />,
                  // <EllipsisOutlined key="ellipsis" />,
                ]
              }
            >
              <Meta
                avatar={<Avatar src={room.ownerHeadPic} />}
                title={room.ownerName}
                description={
                  <div>
                    <p>{room.categoryName}</p>
                    <p>{room.roomName}</p>
                  </div>
                }
              />
            </Card>
          </div>
        ))}
      </div>
    </>
  );
}

export default Recommend;
